import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
    java
}

group = "TestingSpek"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    //Test
    testImplementation(kotlin("reflect", "1.3.21"))
    testImplementation(kotlin("test", "1.3.21"))
    testImplementation(group = "org.spekframework.spek2", name = "spek-dsl-jvm", version = "2.0.0") {
        exclude(group = "org.jetbrains.kotlin")
    }
    testRuntimeOnly(group = "org.spekframework.spek2", name = "spek-runner-junit5", version = "2.0.0") {
        exclude(group = "org.jetbrains.kotlin")
        exclude(group = "org.junit.platform")
    }
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.4.0")
    testImplementation(gradleTestKit())
}

tasks {
    named<Test>("test") {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}